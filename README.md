# Exercice : 
A partir du fichier `nation.sql`, entrainez-vous à réaliser des requêtes SQL. 

## Listez le nombre de continents répertoriés


## Listez les pays d'Afrique, ordonnés par nom
La recherche doit être effectuée par le biais du mot clé "Africa"

## Quel pays a le nom le plus long ? 

## Quels sont les pays ayant un code pays commençant par U.


## Quels sont les pays ayant une fete nationale en mai ?

## Quels sont les pays n'ayant pas de fete nationale ?

## Listez les continents et le nombre de regions associées
La requete doit retourner le nom du continent ainsi que le nombre de régions listées.

## Listez les pays associés à l'antartique
La requete doit retourner le nom de pays listés. Attention la recherche doit être effectuée par le mot clé "Antarctica" et pas par un id.

## Listez les pays ainsi que le nombre de langues officielles associées
Les enregistrements doivent être ordonnés par nombre de langue officielles. Typiquement la suisse ou l'afrique du sud doit être listée en premier. (Les deux ont 4 langues officielles).

## Quel pays a le plus grand gdp en 2016 (le nom du pays doit être affiché)
Le gpd corresponds au Produit Interieur Brut du pays.

## Quel pays a la plus grande population en 2016 (le nom du pays doit être affiché)
## Quels pays ont une population décroissante (La population en 2016 est inferieure à la population en 2012)
## Listez le nom des pays ainsi que le PIB par habitant en l'année 2016, ordonnés par le PIB par habitant en décroissant
## En quel année le PIB par habitant de la france était à son summum ?
## Quelle région est la plus petite du monde ? 
## Quel pays a le plus de langues associées
## Quels sont les 5 languages les plus fréquemment listés en tant que langue officielle
## Y'aurais-t-il un language listé au sein de la table languages non utilisé au sein de la table country_languages ?
## Quels sont les languages risquant de disparaitre dans les prochaines décénies ? (les moins fréquemment listés en tant que language d'un pays). Si plusieurs langues existent, elles doivent être ordonnées par population du pays (derniere année possible)